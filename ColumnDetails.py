##################API and CODE###############################
# from typing import List, Optional
from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()

class ColumnDetailsRequest(BaseModel):
    # dataset location
    inputFileLocation: str
    columnName: str


@app.post("/ColumnDetailsRequestAPI")
# calling the functions with given inputs
async def ColumnDetailsRequestAPI(cd:ColumnDetailsRequest):
    Result = ColumnDetailsdef(cd.inputFileLocation,cd.columnName)
    # ...
    # ...
    return {f"{Result}"}


import pandas as pd
import json

def ColumnDetailsdef(inputFileLocation,columnName):
    df = pd.read_csv(inputFileLocation)
    Total_rows = len(df[columnName])
    print("Total number of values in column :", Total_rows)
    invalid_values = df[columnName].isna().sum()
    valid_values = Total_rows-invalid_values
    print("Number of valid values in column :", valid_values)
    print("Number of invalid/null values in column:", invalid_values)
    unique_values = len(pd.unique(df[columnName]))
    print("Number of unique values in column:", unique_values)
    #empty_values = (df[columnName].values == '').sum()
    empty_values = len(df[df[columnName] == ''])
    print("Number of empty values in column:", empty_values)
    # columns_list = df.columns
    # rows = []
    # for x in columns_list:
    #     Count_Of_Categories_In_Column = df[x].value_counts().count() #list(df[x].value_counts().count())
    #
    #     rows.append([x,Count_Of_Categories_In_Column])
    # print(rows)
    #
    # data = pd.DataFrame(rows, columns=["ColumnName", "Count"])
    # df = data.reset_index()
    # data = df.drop(['index'], axis=1)
    # print(data)
    # data_json = data.to_json(orient="table")  # json.loads(
    # #data_json = data_json1.schema
    # parsed = json.loads(data_json)
    # print(json.dumps(parsed, indent=4))
    # # print(Datatypes_json)
    # return json.dumps(data_json, indent=4, sort_keys=True)
ColumnDetailsdef("data_nan.csv","use_type_id")