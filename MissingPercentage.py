import pandas as pd
import json

def getPctMissing(series):
    num = series.isnull().sum()
    den = series.count()
    return 100*(num/den)

def missingPercentage(inputFileLocation):
    df = pd.read_csv(inputFileLocation)
    #print(df.isnull().sum())
    #print(getPctMissing(df))
    # for i in range(len(df.index)):
    #     print("Nan in row ", i, " : ", df.iloc[i].isnull().sum())
    missingpercentage = getPctMissing(df)
    MP_json = missingpercentage.to_json(orient='index')#json.loads(missingpercentage.to_json(orient='index'))
    print(MP_json)

    return json.dumps(MP_json, indent=4, sort_keys=True)


#missingPercentage("C:\\Users\\kisho\\Desktop\\BinduAllWork\\RIGHTDATA\\DEDUP\\buyerNames_nan.csv")

