##################API and CODE###############################
# from typing import List, Optional
from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()

class ColumnCategoriesRequest(BaseModel):
    # dataset location
    inputFileLocation: str


@app.post("/ColumnCategoriesRequestAPI")
# calling the functions with given inputs
async def ColumnCategoriesRequestAPI(cc:ColumnCategoriesRequest):
    Result = ColumnCategoriesdef(cc.inputFileLocation)
    # ...
    # ...
    return {f"{Result}"}


import pandas as pd
import json

def ColumnCategoriesdef(inputFileLocation):
    df = pd.read_csv(inputFileLocation)
    columns_list = df.columns
    rows = []
    for x in columns_list:
        Count_Of_Categories_In_Column = df[x].value_counts().count() #list(df[x].value_counts().count())

        rows.append([x,Count_Of_Categories_In_Column])
    print(rows)

    data = pd.DataFrame(rows, columns=["ColumnName", "Count"])
    df = data.reset_index()
    data = df.drop(['index'], axis=1)
    print(data)
    data_json = data.to_json(orient="table")  # json.loads(
    #data_json = data_json1.schema
    parsed = json.loads(data_json)
    print(json.dumps(parsed, indent=4))
    # print(Datatypes_json)
    return json.dumps(data_json, indent=4, sort_keys=True)


    # list_Column_Category_count = []

    # # for x in columns_list:
    #     Count_Of_Categories_In_Column = df[x].value_counts().count() #list(df[x].value_counts().count())
    #     #dataframe['Columnn name'].value_counts().count()
    #     list_Column_Category_count.append({x:Count_Of_Categories_In_Column})
    #
    # print(list_Column_Category_count)
    #python_obj = json.loads(list_Column_Category_count)

    # list_Column_Category_count_json1 = list_Column_Category_count.to_json(orient="table")  # json.loads(
    # list_Column_Category_count_json = list_Column_Category_count_json1.schema
    # parsed = json.loads(list_Column_Category_count_json)
    # print(json.dumps(parsed, indent=4))
    # # print(Datatypes_json)
    # return json.dumps(Datatypes_json, indent=4, sort_keys=True)
    #json_string = json.dumps(list_Column_Category_count)


    #return "Count of Categories by column name " +list_Column_Category_count#+json.dumps(python_obj, indent=4, sort_keys=True)

#ColumnCategoriesdef("data_nan.csv")