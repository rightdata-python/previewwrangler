##################API and CODE###############################
# from typing import List, Optional
from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()

class previewDatadefRequest(BaseModel):
    # dataset location
    inputFileLocation: str


@app.post("/previewDatadefRequestAPI")
# calling the functions with given inputs
async def previewDatadefRequestAPI(cc:previewDatadefRequest):
    Result = previewDatadef(cc.inputFileLocation)
    # ...
    # ...
    return {f"{Result}"}


import pandas as pd
import json

def previewDatadef(inputFileLocation):
    data = pd.read_csv(inputFileLocation)
    #data = df.drop(['index'], axis=1)
    #print(data)
    data_json = data.to_json(orient="table")  # json.loads(
    #data_json = data_json1.schema
    parsed = json.loads(data_json)
    print(json.dumps(parsed, indent=4))
    # print(Datatypes_json)
    return json.dumps(data_json, indent=4, sort_keys=True)

previewDatadef("data_nan.csv")